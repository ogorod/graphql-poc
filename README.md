# graphql-poc

**Tech stack**: _node.js, next.js, react, yarn-pnp, yarn 3, typescript, typescript-language-server, eslint, prettier, @next/bundle-analyzer_

```frpc-json-poc``` is based on ```next-app-rh```:

https://gitlab.com/ogorod/next-app-rh

# Usage instructions

```bash
git clone git@gitlab.com:ogorod/frpc-json-poc.git
cd frpc-json-poc
yarn
yarn app:build
yarn app:gate-start # starts front-end web server
yarn app:castle-start # starts GraphQL server
 ```
