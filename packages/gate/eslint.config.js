// Hey Emacs, this is -*- coding: utf-8 -*-

module.exports = {
  extends: [
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'next/core-web-vitals',
  ],
  rules: {
    // /b/; JavaScript
    // /b/{

    'max-len': [
      'error',
      80,
      {
        ignoreUrls: true,
        ignorePattern: '^(?:import|export)\\s.+\\sfrom\\s.+;$',
      },
    ],

    'operator-linebreak': [
      'error',
      'after',
      { overrides: { '?': 'before', ':': 'before' } },
    ],

    // indent: ['error', 2, { flatTernaryExpressions: true }],
    // indent: ['error', 2, { ignoredNodes: ['ConditionalExpression'] }],
    // indent: ['error', 2, { offsetTernaryExpressions: true }],

    // Let tide (or tsc) and js2-mode handle undefined variables
    'no-undef': 'off',
    // 'brace-style': ['warn', 'stroustrup', { allowSingleLine: true }],
    curly: ['warn', 'all'],
    'no-underscore-dangle': [
      'error',
      {
        allowAfterThis: true,
        allow: ['__typename'],
      },
    ],
    'lines-between-class-members': [
      'error',
      'always',
      { exceptAfterSingleLine: true },
    ],
    // 'keyword-spacing': ['error', {
    //   overrides: {
    //     catch: { after: false },
    //     for: { after: false },
    //     if: { after: false },
    //     switch: { after: false },
    //     while: { after: false },
    //   },
    // }],
    'no-empty-function': 'off',
    'no-param-reassign': ['error', { props: false }],
    quotes: ['error', 'single', { avoidEscape: true }],
    'import/prefer-default-export': 'off',
    'max-classes-per-file': 'off',
    // 'arrow-body-style': ['warn', 'as-needed'],
    'arrow-body-style': 'off',
    'implicit-arrow-linebreak': 'off',

    'no-console': 'off',

    // enforce line breaks between braces
    // https://eslint.org/docs/rules/object-curly-newline
    'object-curly-newline': [
      'error',
      {
        ObjectExpression: {
          // minProperties: 4,
          multiline: true,
          consistent: true,
        },
        ObjectPattern: {
          // minProperties: 4,
          multiline: true,
          consistent: true,
        },
        ImportDeclaration: {
          // minProperties: 4,
          multiline: true,
          consistent: true,
        },
        ExportDeclaration: {
          // minProperties: 4,
          multiline: true,
          consistent: true,
        },
      },
    ],

    // /b/}

    // /b/; import
    // /b/{

    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],

    // /b/}

    // /b/; @typescript-eslint
    // /b/{

    '@typescript-eslint/no-empty-function': [
      'error',
      {
        allow: ['constructors'],
      },
    ],

    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': [
      'warn',
      {
        ignoreRestSiblings: true,
        argsIgnorePattern: '^_',
      },
    ],

    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],

    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': 'error',

    'no-useless-constructor': 'off',
    '@typescript-eslint/no-useless-constructor': 'error',

    '@typescript-eslint/explicit-function-return-type': ['error'],

    // /b/}

    // /b/; react
    // /b/{

    'react-hooks/exhaustive-deps': 'warn',
    'react-hooks/rules-of-hooks': 'error',
    'react/destructuring-assignment': 'off',
    'react/forbid-component-props': ['warn', { forbid: ['style'] }],
    'react/forbid-dom-props': ['warn', { forbid: ['style'] }],
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: ['.tsx', '.jsx'],
      },
    ],
    'react/jsx-one-expression-per-line': 'off',
    'react/jsx-pascal-case': 'error',
    'react/jsx-props-no-spreading': 'off',

    // /b/}
  },
  overrides: [
    {
      files: ['*.js', '*.jsx'],
      rules: {
        '@typescript-eslint/explicit-function-return-type': 'off',
      },
    },
  ],
};
