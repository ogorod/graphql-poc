// Hey Emacs, this is -*- coding: utf-8 -*-

// TODO: Write to Zen Desk Garden to add 'export' to IXXLProps and IMDProps

import React, { HTMLAttributes } from 'react';

export interface IXXLProps extends HTMLAttributes<HTMLDivElement> {
  /** Updates the element's HTML tag */
  tag?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  /** Applies bold font style */
  isBold?: boolean;
}

export type XXL = React.FunctionComponent<
  IXXLProps & React.RefAttributes<HTMLDivElement>
>;

interface IMDProps extends HTMLAttributes<HTMLDivElement> {
  /** Updates the element's HTML tag */
  tag?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  /** Applies bold font style */
  isBold?: boolean;
  /** Renders with monospace font */
  isMonospace?: boolean;
}

export type MD = React.FunctionComponent<
  IMDProps & React.RefAttributes<HTMLDivElement>
>;
