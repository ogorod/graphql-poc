// Hey Emacs, this is -*- coding: utf-8 -*-

// https://www.apollographql.com/docs/react/migrating/apollo-client-3-migration/

import { print, GraphQLError } from 'graphql';

import {
  ApolloClient,
  ApolloLink,
  FetchResult,
  HttpLink,
  Observable,
  Operation,
} from '@apollo/client/core';

import { InMemoryCache, NormalizedCacheObject } from '@apollo/client/cache';

import { createClient, ClientOptions, Client } from 'graphql-ws';

import { castleConfig, inBrowser } from '@graphql-poc/iso-share';

import { AppContextValue } from '~/lib/front/app-context';
import { typeDefs, resolvers, data } from '~/client/graphql/schema';

class WebSocketLink extends ApolloLink {
  private client: Client;

  constructor(options: ClientOptions) {
    super();
    // options.keepAlive
    this.client = createClient(options);
  }

  request(operation: Operation): Observable<FetchResult> {
    return new Observable((sink) => {
      return this.client.subscribe<FetchResult>(
        { ...operation, query: print(operation.query) },
        {
          next: sink.next.bind(sink),
          complete: sink.complete.bind(sink),
          error: (err) => {
            // https://github.com/enisdenjo/graphql-ws/issues/105
            //
            // if (err instanceof Event) {
            //   if (err.type === 'error' && err.target instanceof WebSocket) {
            //     console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            //   }
            //   return sink.error(err);
            // }

            if (err instanceof Error) {
              return sink.error(err);
            }

            if (err instanceof CloseEvent) {
              return sink.error(
                // reason will be available on clean closes
                new Error(
                  [
                    `Socket closed with event ${err.code}`,
                    err.reason ? ` ${err.reason}` : '',
                  ].join(),
                ),
              );
            }

            if (err instanceof Event) {
              return sink.error(
                new Error(
                  [
                    `Socket closed with event type "${err.type}"`,
                    `and target === ${err.target}`,
                  ].join(' '),
                ),
              );
            }

            return sink.error(
              // TODO: remove ugly indent disables after moving to eslint
              /* eslint-disable indent */
              Array.isArray(err)
                ? new Error(
                    (err as GraphQLError[])
                      .map(({ message }) => message)
                      .join(', '),
                  )
                : new Error((err as GraphQLError).message),
            );
            /* eslint-enable indent */
          },
        },
      );
    });
  }
}

const createApolloClient = (
  _context: AppContextValue,
): ApolloClient<NormalizedCacheObject> => {
  let link;

  if (inBrowser()) {
    const { wsProtocol, wsPort, wsPath } = castleConfig;
    let wsHost;
    if (castleConfig.wsClientHost === 'localhost') {
      wsHost = window.location.hostname;
    } else {
      wsHost = castleConfig.wsHost;
    }
    link = new WebSocketLink({
      url: `${wsProtocol}//${wsHost}:${wsPort}${wsPath}`,
      lazy: false,
      // keep retrying while the browser is open
      retryAttempts: Infinity,
      connectionAckWaitTimeout: 3000,
    });
  } else {
    const { httpProtocol, httpHost, httpPort, httpPath } = castleConfig;
    link = new HttpLink({
      uri: `${httpProtocol}//${httpHost}:${httpPort}${httpPath}`,
    });
  }

  const cache = new InMemoryCache();

  const client = new ApolloClient({ link, cache, resolvers, typeDefs });

  cache.writeQuery({
    ...data.gateCounter,
  });

  return client;
};

let apolloClient: ApolloClient<NormalizedCacheObject> | null = null;

export const getApolloClient = (
  context: AppContextValue,
): ApolloClient<NormalizedCacheObject> => {
  if (apolloClient === null) {
    apolloClient = createApolloClient(context);
  }

  return apolloClient;
};
