/* eslint-disable */
// This file is generated with graphql-codegen. see ./codegen.yaml
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
import { FieldPolicy, FieldReadFunction, TypePolicies, TypePolicy } from '@apollo/client/cache';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type GateCounter = {
  __typename?: 'GateCounter';
  count: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  gateCounterDecrement: GateCounter;
  gateCounterIncrement: GateCounter;
};

export type Query = {
  __typename?: 'Query';
  gateCounter: GateCounter;
};

export type GetGateCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetGateCountQuery = { __typename?: 'Query', gateCounter: { __typename: 'GateCounter', count: number } };


export const GetGateCountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetGateCount"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounter"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GetGateCountQuery, GetGateCountQueryVariables>;
export type GateCounterKeySpecifier = ('count' | GateCounterKeySpecifier)[];
export type GateCounterFieldPolicy = {
	count?: FieldPolicy<any> | FieldReadFunction<any>
};
export type MutationKeySpecifier = ('gateCounterDecrement' | 'gateCounterIncrement' | MutationKeySpecifier)[];
export type MutationFieldPolicy = {
	gateCounterDecrement?: FieldPolicy<any> | FieldReadFunction<any>,
	gateCounterIncrement?: FieldPolicy<any> | FieldReadFunction<any>
};
export type QueryKeySpecifier = ('gateCounter' | QueryKeySpecifier)[];
export type QueryFieldPolicy = {
	gateCounter?: FieldPolicy<any> | FieldReadFunction<any>
};
export type StrictTypedTypePolicies = {
	GateCounter?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | GateCounterKeySpecifier | (() => undefined | GateCounterKeySpecifier),
		fields?: GateCounterFieldPolicy,
	},
	Mutation?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | MutationKeySpecifier | (() => undefined | MutationKeySpecifier),
		fields?: MutationFieldPolicy,
	},
	Query?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | QueryKeySpecifier | (() => undefined | QueryKeySpecifier),
		fields?: QueryFieldPolicy,
	}
};
export type TypedTypePolicies = StrictTypedTypePolicies & TypePolicies;