// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { MD, XXL } from '@zendeskgarden/react-typography';

export const H1 = styled(
  XXL as import('~/types/zendeskgarden-patch').XXL,
).attrs({
  isBold: true,
  tag: 'h1',
})`
  margin-bottom: ${(props): string => props.theme.fontSizes.sm};
`;

export const P = styled(MD as import('~/types/zendeskgarden-patch').MD)`
  margin-top: ${(props): string => props.theme.space.xxs};
`;
