/* eslint-disable */
// This file is generated with graphql-codegen. see ./codegen.yaml
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type CastleCounter = {
  __typename?: 'CastleCounter';
  count: Scalars['Float'];
};

export type GateCounter = {
  __typename?: 'GateCounter';
  count: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  castleCounterDecrement: CastleCounter;
  castleCounterIncrement: CastleCounter;
  gateCounterDecrement: GateCounter;
  gateCounterIncrement: GateCounter;
};

export type Query = {
  __typename?: 'Query';
  castleCounter: CastleCounter;
  gateCounter: GateCounter;
  sayHello: Scalars['String'];
};


export type QuerySayHelloArgs = {
  clientId: Scalars['String'];
  myMame: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  castleCounterChanged: CastleCounter;
};

export type SayHelloQueryVariables = Exact<{ [key: string]: never; }>;


export type SayHelloQuery = { __typename?: 'Query', sayHello: string };

export type GetCastleCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCastleCountQuery = { __typename?: 'Query', castleCounter: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterChangedSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterChangedSubscription = { __typename?: 'Subscription', castleCounterChanged: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterIncrementMutationVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterIncrementMutation = { __typename?: 'Mutation', castleCounterIncrement: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterDecrementMutationVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterDecrementMutation = { __typename?: 'Mutation', castleCounterDecrement: { __typename?: 'CastleCounter', count: number } };

export type GetGateCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetGateCountQuery = { __typename?: 'Query', gateCounter: { __typename?: 'GateCounter', count: number } };

export type GateCounterIncrementMutationVariables = Exact<{ [key: string]: never; }>;


export type GateCounterIncrementMutation = { __typename?: 'Mutation', gateCounterIncrement: { __typename?: 'GateCounter', count: number } };

export type GateCounterDecrementMutationVariables = Exact<{ [key: string]: never; }>;


export type GateCounterDecrementMutation = { __typename?: 'Mutation', gateCounterDecrement: { __typename?: 'GateCounter', count: number } };


      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {}
};
      export default result;
    

export const SayHelloDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SayHello"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"sayHello"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"myMame"},"value":{"kind":"StringValue","value":"rh","block":false}},{"kind":"Argument","name":{"kind":"Name","value":"clientId"},"value":{"kind":"StringValue","value":"one","block":false}}]}]}}]} as unknown as DocumentNode<SayHelloQuery, SayHelloQueryVariables>;
export const GetCastleCountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetCastleCount"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounter"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GetCastleCountQuery, GetCastleCountQueryVariables>;
export const CastleCounterChangedDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"subscription","name":{"kind":"Name","value":"CastleCounterChanged"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterChanged"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterChangedSubscription, CastleCounterChangedSubscriptionVariables>;
export const CastleCounterIncrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CastleCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterIncrementMutation, CastleCounterIncrementMutationVariables>;
export const CastleCounterDecrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CastleCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterDecrementMutation, CastleCounterDecrementMutationVariables>;
export const GetGateCountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetGateCount"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounter"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GetGateCountQuery, GetGateCountQueryVariables>;
export const GateCounterIncrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"GateCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounterIncrement"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GateCounterIncrementMutation, GateCounterIncrementMutationVariables>;
export const GateCounterDecrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"GateCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounterDecrement"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GateCounterDecrementMutation, GateCounterDecrementMutationVariables>;