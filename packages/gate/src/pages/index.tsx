// Hey Emacs, this is -*- coding: utf-8 -*-

import { useEffect, useState } from 'react';

import type { NextPage } from 'next';

import { useApolloClient, useMutation, useQuery } from '@apollo/client';

import Head from 'next/head';

import { Grid, Row, Col } from '@zendeskgarden/react-grid';
import { Button } from '@zendeskgarden/react-buttons';

import { ButtonsRow } from '~/components/styled/grid';
import { H1, P } from '~/components/styled/typography';

import {
  CastleCounterChangedDocument,
  CastleCounterDecrementDocument,
  CastleCounterIncrementDocument,
  GateCounterDecrementDocument,
  GateCounterIncrementDocument,
  GetCastleCountDocument,
  GetGateCountDocument,
  SayHelloDocument,
} from '~/lib/front/graphql-operations';

const Home: NextPage = () => {
  const [state, setState] = useState<{
    helloResults: string[];
  }>({
    helloResults: [],
  });

  const apolloClient = useApolloClient();

  const {
    data: castleCountData,
    subscribeToMore: castleCountSubscribeToMore,
    refetch: castleCountRefetch,
  } = useQuery(GetCastleCountDocument, {
    ssr: false,
    errorPolicy: 'all',
  });

  useEffect(() => {
    let unsubscribe: ReturnType<typeof castleCountSubscribeToMore> | undefined;

    const subscribe = (): ReturnType<typeof castleCountSubscribeToMore> => {
      return castleCountSubscribeToMore({
        document: CastleCounterChangedDocument,
        updateQuery: (prev, { subscriptionData: { data } }) => {
          if (!data) {
            return prev;
          }
          return {
            ...prev,
            castleCounter: {
              ...prev.castleCounter,
              count: data.castleCounterChanged.count,
            },
          };
        },
        onError: (_error) => {
          if (unsubscribe) {
            unsubscribe();
          }
          castleCountRefetch();
          unsubscribe = subscribe();
        },
      });
    };

    unsubscribe = subscribe();

    return (): void => {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, [castleCountRefetch, castleCountSubscribeToMore]);

  const [castleCounterIncrement] = useMutation(CastleCounterIncrementDocument);
  const [castleCounterDecrement] = useMutation(CastleCounterDecrementDocument);

  const { data: gateCountData } = useQuery(GetGateCountDocument, {
    ssr: false,
    errorPolicy: 'all',
  });

  const [gateCounterIncrement] = useMutation(GateCounterIncrementDocument);
  const [gateCounterDecrement] = useMutation(GateCounterDecrementDocument);

  return (
    <>
      <Head>
        <title>GraphQL POC</title>
        <meta name="description" content="GraphQL POC" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Grid>
          <Row>
            <Col>
              <H1>Useless Counters</H1>
              <P>Castle Count: {castleCountData?.castleCounter?.count}</P>
              <ButtonsRow>
                <Button
                  type="button"
                  onClick={(_event): void => {
                    castleCounterIncrement();
                  }}
                >
                  Up!
                </Button>
                <Button
                  type="button"
                  onClick={(_event): void => {
                    castleCounterDecrement();
                  }}
                >
                  Down!
                </Button>
              </ButtonsRow>
              <p>Gate Count: {gateCountData?.gateCounter?.count}</p>
              <ButtonsRow>
                <Button
                  type="button"
                  onClick={(_event): void => {
                    gateCounterIncrement();
                  }}
                >
                  Up!
                </Button>
                <Button
                  type="button"
                  onClick={(_event): void => {
                    gateCounterDecrement();
                  }}
                >
                  Down!
                </Button>
              </ButtonsRow>
            </Col>
            <Col>
              <H1>Hello Requester!</H1>
              <ButtonsRow>
                <Button
                  onClick={async (_event): Promise<void> => {
                    const { data } = await apolloClient.query({
                      query: SayHelloDocument,
                      errorPolicy: 'all',
                      fetchPolicy: 'no-cache',
                    });

                    setState((prevState) => {
                      const helloResults = [...prevState.helloResults];
                      helloResults.push(data.sayHello);

                      return {
                        ...prevState,
                        helloResults,
                      };
                    });
                  }}
                >
                  Say Hello!
                </Button>
              </ButtonsRow>
              <div>
                {state.helloResults.map((value, index) => (
                  <P key={index.toString()}>{value}</P>
                ))}
              </div>
            </Col>
          </Row>
        </Grid>
      </main>
    </>
  );
};

export default Home;
