// Hey Emacs, this is -*- coding: utf-8 -*-

/*
eslint @typescript-eslint/no-unused-vars: [
  warn, { argsIgnorePattern: 'type|returns|of' }
]
*/

/*
eslint no-underscore-dangle: [
  'error', {
    allowAfterThis: true,
    allow: ['_key', '_id', '_rev', '_to', '_from', '_db'],
  }
]
*/

/* eslint-disable class-methods-use-this */
/* eslint-disable max-classes-per-file */
/* eslint-disable arrow-parens */
/* eslint-disable @typescript-eslint/no-use-before-define */

import 'reflect-metadata';

// import fs from 'fs';
// import path from 'path';

// import { GraphQLFormattedError } from 'graphql';
import express from 'express';

// For free TLS certificates at LetsEncrypt see e.g.:
// https://itnext.io/node-express-letsencrypt-generate-a-free-ssl-certificate-and-run-an-https-server-in-5-minutes-a730fbe528ca
// import https from 'https';
import http from 'http';

import { graphqlHTTP } from 'express-graphql';
import uWS from 'uWebSockets.js';
import { makeBehavior } from 'graphql-ws/lib/use/uWebSockets';

import {
  Arg,
  Field,
  Mutation,
  ObjectType,
  PubSub,
  Publisher,
  Query,
  Resolver,
  Root,
  Subscription,
  buildSchema,
} from 'type-graphql';

import { castleConfig } from '@graphql-poc/iso-share';

// import { projectRoot } from '~/project-root';

// TODO: Will try to use context object first - typedi might be not required.
// import { Container, Inject, Service } from 'typedi';

// @Service()
export class Fid50 {}

type Context = {};

@ObjectType()
class CastleCounter {
  constructor() {
    this.count = 0;
  }

  @Field()
  count: number;
}

// @Service()
@Resolver()
class Castle {
  // constructor(
  //   // @Inject(() => Fid50)
  //   private readonly fid50: Fid50,
  // ) {}

  @Query((_returns) => String)
  async sayHello(
    @Arg('myMame') myMame: string,
    @Arg('clientId') clientId: string,
  ): Promise<string> {
    const now = new Date();

    return `${now.toISOString()} Hello ${myMame} @ ${clientId}`;
  }

  /// /b/; castleCounter
  /// /b/{

  @Query((_returns) => CastleCounter)
  async castleCounter(): Promise<CastleCounter> {
    return this._counter;
  }

  @Mutation((_returns) => CastleCounter)
  async castleCounterIncrement(
    @PubSub('castleCounterChanged') publish: Publisher<CastleCounter>,
  ): Promise<CastleCounter> {
    console.log('in castleCounterIncrement()');
    this._counter.count += 1;
    await publish(this._counter);
    return this._counter;
  }

  @Mutation((_returns) => CastleCounter)
  async castleCounterDecrement(
    @PubSub('castleCounterChanged') publish: Publisher<CastleCounter>,
  ): Promise<CastleCounter> {
    console.log('in castleCounterDecrement()');
    this._counter.count -= 1;
    await publish(this._counter);
    return this._counter;
  }

  @Subscription({ topics: 'castleCounterChanged' })
  castleCounterChanged(@Root() payload: CastleCounter): CastleCounter {
    console.log('in castleCounterChanged()');
    return payload;
  }

  private _counter = new CastleCounter();

  /// /b/}
}

export default async function main(): Promise<void> {
  const schema = await buildSchema({
    resolvers: [Castle],
    // container: Container,
  });

  const {
    wsProtocol,
    wsHost,
    wsPort,
    wsPath,
    httpProtocol,
    httpHost,
    httpPort,
    httpPath,
  } = castleConfig;

  const httpUrl = `${httpProtocol}//${httpHost}:${httpPort}${httpPath}`;
  const wsUrl = `${wsProtocol}//${wsHost}:${wsPort}${wsPath}`;

  const app = express();
  app.use(
    httpPath,
    graphqlHTTP({
      schema,
      graphiql: true,
      // pretty: true,
      // TODO: as of express-graphql@0.12.0, subscriptions in graphiql do not
      //       work, check it at next versions.
      // graphiql: {
      //   subscriptionEndpoint: `${wsHref}`,
      // },
    }),
  );

  // const key = fs.readFileSync(
  //   path.join(projectRoot, 'certs/selfsigned.key'),
  //   'ascii',
  // );

  // const cert = fs.readFileSync(
  //   path.join(projectRoot, 'certs/selfsigned.crt'),
  //   'ascii',
  // );

  // const options = { key, cert };

  // const server = https.createServer(options, app);

  const server = http.createServer(app);

  server.listen(httpPort, () => {
    console.log(`HTTP server ready at ${httpUrl}`);

    uWS
      .App()
      .ws(castleConfig.wsPath, makeBehavior({ schema }))
      .listen(castleConfig.wsPort, (listenSocket) => {
        if (listenSocket) {
          console.log(`WebSockets server ready at ${wsUrl}`);
        }
      });
  });
}
