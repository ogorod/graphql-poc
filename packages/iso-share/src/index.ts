// Hey Emacs, this is -*- coding: utf-8 -*-

export const inBrowser = (): boolean => {
  return typeof window !== 'undefined';
};

export interface WebSocketConnectionParams {
  // The same meaning as in HTTP authorization header
  authorization: string;
}

export const castleConfig = {
  httpProtocol: 'http:',
  httpHost: 'localhost',
  httpPort: 4000,
  httpPath: '/graphql',
  wsProtocol: 'ws:',
  wsHost: 'localhost',
  wsPort: 5000,
  wsPath: '/graphql',
  // wsClientHost can be different from wsHost.
  // If wsClientHost ie equal "localhost", the client is going to use its
  // window.location.hostname to find its ws address.
  // Otherwise, the client will use wsClientHost value as it is.
  wsClientHost: 'localhost',
};
